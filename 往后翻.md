### 美国金刚公司
<img src="/kkLOGO/kkLogoSmall.PNG" alt="kk Logo" width="100"/>

<!-- ![image](/kkLOGO/kkLogoSmall.PNG) -->

## 免费安全自由上网
## 首选可信美制金刚
- <strong>特大喜讯：金刚app——免费安全自由上网、无限流量高速通畅</strong>

### 通知
22.11.11-21 金刚app后台系统遭受攻击 - 问题已经解决 用户需要升级到新版app
- 安卓app 4.5 或更新 按本页下面链接下载安装
- 苹果app 3.3.1 或更新 按本页下面链接或去苹果app商店

### <font color green>绿色通道</font>
- 若持<strong>  安卓类手机/平板 </strong>（如华为、OPPO、VIVO、小米、三星等）
   - <strong>首选金刚app</strong>
     - 请用以下3种方法之一下载安装最新版金刚app
       - [谷歌app商店](https://play.google.com/store/apps/details?id=com.kk.androidclient.kkapp)——中国大陆用户无法免梯访问
       - [apkpure app 商店](https://apkpure.com/p/com.kk.androidclient.kkapp)
       - <strong>跳过app商店直接下载安装——适用于中国大陆用户</strong>
         - 请点击以下两行蓝色文字之一下载安装
           - [下载链接1](https://bitbucket.org/kk64/public/downloads/app-prod-release.apk)
           - [下载链接2](https://github.com/a2zitpro/client/releases/download/latest/app-prod-release.apk)
          - 如果点击无响应，请按以下之一做
            - 长按以上蓝色文字，选<strong> 复制链接网址 </strong>再粘贴到谷歌浏览器、火狐浏览器地址栏点 前往
            - 请在 另一个浏览器 的地址栏里严格按大小写录入以下字母数字，然后点 前往，再点以上蓝色文字即可下载
               - cutt.ly/xxqCMtF
            - 在金刚app里，选择截屏分享屏二维码，扫码，在浏览器里打开本网页即可继续下载 
          - ⚠️<font color="red">微信用户</font>
             - 点右上角 •••
             - 选<strong> 在浏览器中打开 </strong>
             - 选默认浏览器，或指定另一个浏览器
             - 点击以上蓝色文字即可下载
             - 下载时请选<strong> 直接下载 </strong>，不要用<strong> 腾讯应用宝 </strong>下载
             - 当点击以上蓝色文字无法下载时，请长按蓝色文字，选<Strong> 复制链接网址</Strong>，将其粘贴到您信任的安全浏览器——比如Chrome——的地址栏中，再点<strong> 前往 </strong>下载
   - <strong>次选金刚号</strong>
     - 金刚号
       - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
     - 如何获取、配置金刚号
       - 请在 安卓手机 上安装金刚app
       - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
       - 按照配置方法配入安卓类手机/平板 即可自由上网
       <br>
- 若持<strong> 苹果手机/平板</strong>
  - <strong>首选金刚号</strong>
    - 金刚号
      - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
     - 如何获取、配置金刚号
       - 请用自己的、亲人的、或借可信赖的朋友的安卓手机按上一节指引安装金刚app
       - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
       - 按照配置方法配入苹果设备即可自由上网
  - <strong>次选金刚app </strong>
     - 若您已有苹果公司台湾区 或 美国区的 Apple ID，请点击以下链接之一下载安装金刚app
       - [苹果台湾店](https://apps.apple.com/tw/app/kkvpn/id1530649322)
       - [苹果美国店](https://apps.apple.com/us/app/kkvpn/id1530649322)
     - 若您没有苹果公司台湾区 或 美国区的Apple ID
       - 请[ 点击这里 ](/LadderFree/kkDictionary/kkAppLadder/iOS/CreatAppleIDofTaiwan.md)注册台湾区 Apple ID
       - 请[ 点击这里 ](/LadderFree/kkDictionary/kkAppLadder/iOS/CreatAppleIDofAmeric.md)注册美国区 Apple ID
       - 再到台湾 或 美国苹果App商店搜 <strong>kkVPN</strong>，或点击以上美国 或 台湾苹果App店链接下载安装
      - 以下教程<Strong> 已经来自中国大陆区的金刚之友验证有效可行 </Strong>
        - [注册台湾区 Apple ID 教程 ](/LadderFree/kkDictionary/kkAppLadder/iOS/CreatAppleIDofTaiwan.md)
  <br>
- 若持<strong> Windows</strong>
   - <strong>仅选金刚号</strong>
     - 金刚号
       - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
     - 如何获取、配置金刚号
       - 请按以上教程在 安卓手机 或 苹果手机 上安装金刚app
       - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
       - 按照配置方法配入Windows计算机即可自由上网
  
  <br>
- 若持<strong> 苹果电脑Mac</strong>
   - <strong>仅选金刚号</strong>
     - 金刚号
       - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
     - 如何获取、配置金刚号
       - 请按以上教程在 安卓手机 或 苹果手机 上安装金刚app
       - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
       - 按照配置方法配入苹果电脑Mac即可自由上网
  <br>
- 若持<strong> 安卓类机顶盒</strong>（如小米机顶盒等）
  - <strong>首选金刚号</strong>
    - 金刚号
      - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
    - 如何获取、配置金刚号
      - 请按以上教程在 安卓手机 或 苹果手机 上安装金刚app
      - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
      - 按照配置方法配入安卓机顶盒即可自由上网
  - <strong>次选金刚app</strong>
    - [点击这里 ](/LadderFree/Android/TVBox/KKLadderAPP/KKLadderAPPConfigure.md)下载安装金刚app最新版
  <br>
- 若持<strong> 安卓类路由器</strong>
   - <strong>仅选金刚号</strong>
     - 金刚号
       - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
     - 如何获取、配置金刚号
       - 请按以上教程在 安卓手机 或 苹果手机 上安装金刚app
       - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
       - 按照配置方法配入安卓路由器即可自由上网
  <br>

- 若持<strong> Linux</strong>
   - <strong>仅选金刚号</strong>
     - 金刚号
       - 金刚号是A开头的一串数字。金刚参数是包括金刚号在内的4条参数。把金刚参数按配置说明填入您的设备，只要有共享流量供金刚号使用，即可自由上网。不必下载安装app
     - 如何获取、配置金刚号
       - 请按以上教程在 安卓手机 或 苹果手机 上安装金刚app
       - 再在 app的我屏 获取 免费通用金刚号 和 配置方法
       - 按照配置方法配入Linux计算机即可自由上网

<br>
———————————————
<br>
<br>


## 自由上网 神器金刚

- [ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)精心打造
- 视频流畅、画质清晰
- 基于美国法律的[ 用户信息和用户隐私保护 ](/LadderFree/kkDictionary/KKEnduserContract.md)
- 易用好用
- 安全
  - 金刚在您的设备和金刚服务器之间建立一个加密安全通道，对往来数据高级别加密，以保护您的网上隐私
  - 确保黑客、WiFi&4G信号提供商及其他可能怀有恶意的自然人、机构无法窥探您的个人信息、网上隐私、上网习惯
  - 金刚将使您当地的WiFi&4G信号提供商及第三方无法知晓您正在、曾经访问什么网站
  - 金刚将隐藏您的设备的IP地址，把金刚服务器的IP地址展示给您所访问的网站，这将有助于帮助您匿名上网：您所访问的网站无法知晓您来自何处、您是何人
  - 在家中或借助公共场所Wi-Fi上网时，请务必用金刚，以确保您安全上网
 
金刚提供两类产品：金刚app 和 金刚号，可覆盖所有主流智能设备：您只需选其中一种，即可突破长城防火墙：

#### 一. 金刚app
- 特色
  - 0键连接
  - 安全高速 
  - 免费无限流量，付费服务更快更好用
  - 基于HTTPS/SSL VPN 
  - 企业级安全加密
- 适用设备
  - 安卓系列
    - 安卓类手机
    - 安卓类型平板
    - 安卓机类顶盒
  - 苹果系列
    - 苹果手机
    - 苹果平板
- 免费使用金刚
  - 安卓系列
    - 金刚app>=3.0版免费无限流量
  - 苹果系列
    - 金刚app>=2.0版免费无限流量
#### 二、金刚号
- 特色

  - 不必下载app 
    - 将3～4条参数配入以下智能设备（简称：设备）即可
  - 一号在手，全家自由
    - 每1枚通用金刚号可配入无数部设备
  - 一拖九
    - 配入同1个通用金刚号的以下设备中，可有9部同时连通

- 适用设备
  - 安卓类手机/平板/机顶盒/路由器
    - 通用金刚号/收费
      - 每天供应少量试用免费流量
  - 苹果手机/平板/电脑Mac
    - 通用金刚号/收费
      - 每天供应少量试用免费流量
  - Windows
    - 通用金刚号/收费
      - 每天供应少量试用免费流量
    - 专用金刚号+SSL客户端/免费
    - 通用金刚号+SSL客户端/收费
      - 每天供应少量试用免费流量
  - Linux
    - 通用金刚号/收费
      - 每天供应少量试用免费流量
- [金刚流量价格](/LadderFree/kkDictionary/Price/KKDTPrice.md)
  - [常速通用金刚号流量价格](/LadderFree/kkDictionary/Price/KKDTPriceOfKKID_SpeedLevel01.md)
  - [高速通用金刚号流量价格](/LadderFree/kkDictionary/Price/KKDTPriceOfKKID_SpeedLevel02.md)

- [电邮客服 ](mailto:cs@a2zitpro.com)获取金刚号


###### 玩转金刚梯>金刚字典>
### 金刚伙伴
- 所谓<Strong> 金刚伙伴 </Strong>是指分享[ 金刚梯 ](/LadderFree/kkDictionary/KKLadder.md)给新人，获取某种报酬的[ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)合作者
- <Strong> 金刚伙伴 </Strong>共两类
  - [ 金刚广告商 ](/LadderFree/kkDictionary/KKAdvertiser.md)
  - [ 金刚推荐人 ](/LadderFree/kkDictionary/KKReferrer.md)

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)




###### 玩转金刚梯>金刚字典>

### 金刚号回收
- 处于[ 流量过期 ](/LadderFree/kkDictionary/KKDataTrafficExpiredMark.md)状态满3个月、且期间未连通1次的[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)，将被[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)自动回收
- 期间，[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)将每月给号主发一封为该号购买[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)的提醒邮件
- 该号被回收后，[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)将停止向号主发送提醒邮件

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)



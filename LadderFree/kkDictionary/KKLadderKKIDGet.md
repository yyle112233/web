###### 玩转金刚>金刚字典>
### 获取金刚号梯

- 1、如果您尚未在[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)[ 注册 ](/LadderFree/kkDictionary/Registration.md)，则请

  - [<strong> 点击这里 </strong>](/LadderFree/kkDictionary/RegisterDIY.md)

  - 阅读并照做，将获得第1架[ 万能金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKIDMultipurpose.md)

- 2、如果您已经在[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)[ 注册 ](/LadderFree/kkDictionary/Registration.md)，且需更多[ 金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKID.md)，则请

  - [<strong> 点击这里 </strong>](/LadderFree/kkDictionary/KKLadderKKIDGetMore.md)

  - 阅读并照做，将获得更多[ 金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKID.md)


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
